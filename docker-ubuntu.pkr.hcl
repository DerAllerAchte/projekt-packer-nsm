packer {
  required_plugins {
    vagrant = {
      version = "~> 1"
      source  = "github.com/hashicorp/vagrant"
    }

    virtualbox = {
      version = "~> 1"
      source  = "github.com/hashicorp/virtualbox"
    }
  }
}

source "virtualbox-iso" "basic-example" {
  guest_os_type    = "Ubuntu_64"
  iso_url          = "https://releases.ubuntu.com/jammy/ubuntu-22.04.3-live-server-amd64.iso"
  iso_checksum     = "sha256:a4acfda10b18da50e2ec50ccaf860d7f20b389df8765611142305c0e911d16fd"
  http_directory   = "http"
  boot_command     = [
    "c",
    "linux /casper/vmlinuz autoinstall ", # fehler boot command -> Blog
    "ds='nocloud-net;s=http://{{.HTTPIP}}:{{.HTTPPort}}/' --- <enter><wait>",
    "initrd /casper/initrd<enter><wait>",
    "boot<enter>"
  ]
  boot_wait        = "10s"
  communicator     = "ssh"
  vm_name          = "nsm-ubuntu" 
  cpus             = "2"
  memory           = "2046"
  disk_size        = "81920"
  headless         = false
  ssh_username     = "vagrant"
  ssh_password     = "vagrant"
  ssh_timeout      = "100m"
  guest_additions_mode = "upload"
  shutdown_command = "echo 'vagrant' | sudo -S shutdown -P now"
}

build {
  sources = ["sources.virtualbox-iso.basic-example"]


#   provisioner "shell" {
#   name = "Disable Firewall"
#   inline = ["ufw disable"]  # Replace with your firewall command if different
# }

  provisioner "shell" {
    inline = [
      "export DEBIAN_FRONTEND=noninteractive",
      "apt install apache2 --yes",
      "apt install tree --yes",
      "echo 'vagrant ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/vagrant",

    ]
    execute_command = "echo 'vagrant' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"
  }

  post-processor "vagrant" {
    keep_input_artifact = false
    compression_level   = 0
    output              = "ubuntu.box"
  }
}