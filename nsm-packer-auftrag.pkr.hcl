packer {
  required_plugins {
    vagrant = {
      version = "~> 1"
      source  = "github.com/hashicorp/vagrant"
    }

    virtualbox = {
      version = "~> 1"
      source  = "github.com/hashicorp/virtualbox"
    }
  }
}

source "virtualbox-iso" "basic-example" {
  guest_os_type    = "Ubuntu_64"
  iso_url          = "https://releases.ubuntu.com/jammy/ubuntu-22.04.3-live-server-amd64.iso"
  iso_checksum     = "sha256:a4acfda10b18da50e2ec50ccaf860d7f20b389df8765611142305c0e911d16fd"
  http_directory   = "http"
  boot_command     = [
    "c",
    "linux /casper/vmlinuz autoinstall ",
    "ds='nocloud-net;s=http://{{.HTTPIP}}:{{.HTTPPort}}/' --- <enter><wait>",
    "initrd /casper/initrd<enter><wait>",
    "boot<enter>"
  ]
  boot_wait        = "10s"
  communicator     = "ssh"
  vm_name          = "nsm-ubuntu" 
  cpus             = "2"
  memory           = "2046"
  disk_size        = "81920"
  headless         = false
  ssh_username     = "vagrant"
  ssh_password     = "vagrant"
  ssh_port         = "22"
  ssh_timeout      = "100m"
  shutdown_command = "echo 'vagrant' | sudo -S shutdown -P now"

vboxmanage = [
    ["modifyvm", "{{.Name}}", "--nat-localhostreachable1", "on"],
  ]


}

build {
  sources = ["sources.virtualbox-iso.basic-example"]


#   provisioner "shell" {
#   name = "Disable Firewall"
#   inline = ["ufw disable"]  # Replace with your firewall command if different
# }

  provisioner "shell" { 
    inline = [  
      "export DEBIAN_FRONTEND=noninteractive",  
      "apt install apache2 --yes", 
      "apt install build-essential dkms linux-headers-$(uname -r) --yes", 
      "apt install tree --yes", 
      "mkdir -p /home/vagrant/.ssh",  
      "chmod 0700 /home/vagrant/.ssh",  
      "curl https://raw.githubusercontent.com/hashicorp/vagrant/main/keys/vagrant.pub -o /home/vagrant/.ssh/authorized_keys", 
      "chmod 0600 /home/vagrant/.ssh/authorized_keys",  
      "wget http://download.virtualbox.org/virtualbox/4.3.8/VBoxGuestAdditions_4.3.8.iso",
      "mkdir /media/VB",
      "mount -o loop -t iso9660 /home/vagrant/VBoxGuestAdditions_4.3.8.iso /media/VB",
      "sh /media/VB/VBoxLinuxAdditions.run",
      "rm VBoxGuestAdditions_4.3.8.iso",
      "rcvboxadd reload",
      "umount /media/VB",
      "rmdir /media/VB"
    ] 
    execute_command = "echo 'vagrant' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"  
  } 
  
  post-processor "vagrant" {  
    keep_input_artifact = false 
    compression_level   = 0 
    output              = "peach.box" 
  }
} 